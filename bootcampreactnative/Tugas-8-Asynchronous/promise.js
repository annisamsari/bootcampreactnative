function readBooksPromise(time, books) {
    console.log(`saya mulai membaca ${book.name}`);
    return new Promise(function (resolve, reject) {
        setTimeout(() => {
            let sisaWaktu = time - book.timeSpent;
            if (sisaWaktu >= 0) {
                console.log(
                    `saya sudah selesai membaca ${book.name}, sisa waktu saya ${sisaWaktu}`
                );
                resolve (sisaWaktu);
            }
        }, book.timeSpent);
    });
}

module.exports = readBooksPromise