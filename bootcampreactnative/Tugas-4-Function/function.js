//Soal 1
console.log("Soal Nomor 1")

function teriak() {
    console.log("Halo Sanbers!");
  }
   
  teriak(); 

  console.log("\n")
//Soal 2
console.log("Soal Nomor 2")

var num1 = 12
var num2 = 4
function kalikan() {
    return num1 * num2
}
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

console.log("\n")
//Soal 3
console.log("Soal Nomor 3")

var nama = "Agus";
var umur = 30;
var alamat = "Jln. Malioboro, Yogyakarta";
var hobi = "Gaming";
function introduce() {
    return "Nama saya " + nama + "," + " umur saya " + umur + " tahun, alamat saya di " + alamat + ", hobi saya adalah " + hobi 
}
var perkenalan = introduce("Nama saya " + nama + "," + " umur saya " + umur + " tahun, alamat saya di " + alamat + ", hobi saya adalah " + hobi)
console.log(perkenalan);