// nama = "" dan peran = ""
var nama = ""
var peran = ""

if ( nama == "") {
    console.log("Nama harus diisi!");
}
if ( peran == "") {
    console.log("Pilih peranmu untuk memulai game!")
}

// nama = Vian dan peran = ""
var nama = "Vian"
var peran = ""

if ( nama == "Vian" && peran == "" ) {
    console.log("Halo Vian, Pilih peran untuk memulai game!");
} else {
    console.log("Nama harus diisi! Pilih peran untuk memulai game!")
}

// nama = "Jane" dan peran = "Penyihir"
var nama = "Jane"
var peran = "Penyihir"

if ( nama === "Jane" ) {
    console.log("Selamat datang di Dunia Werewolf, Jane")
}
if ( peran == "Penyihir") {
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
}

// nama = "Jenita" dan peran = "Guard"
var nama = "Jenita"
var peran = "Guard"

if ( nama === "Jenita" ) {
    console.log("Selamat datang di Dunia Werewolf, Jenita")
}
if ( peran === "Guard" ) {
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
}

// nama = "Junaedi" dan peran = "Werewolf"
var nama = "Junaedi"
var peran = "Werewolf"

if ( nama === "Junaedi" ) {
    console.log("Selamat datang di Dunia Werewolf, Junaedi")
}
if ( peran === "Werewolf") {
    console.log("Halo Werewolf Junaedi, kamu akan memakan mangsa setiap malam!")
}


// Switch Case
var hari = 19; 
var bulan = 1; 
var tahun = 2005;

switch(bulan) {
    case 1: { console.log("19 Januari 2005"); break; }
    case 2: { console.log("19 Januari 2005"); break; }
    default : { console.log("tidak terjadi apa-apa"); break; }
}