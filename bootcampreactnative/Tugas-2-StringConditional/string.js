// soal 1
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

console.log(word + ' ' + second + ' ' + third + ' ' + fourth + ' ' + fifth + ' ' + sixth + ' ' + seventh)

// soal 2
var sentence = "I am going to be React Native Developer"; 

var exampleFirstWord = sentence[0] ; 
var exampleSecondWord = sentence[2] + sentence[3]  ; 
var examplethirdWord = sentence.substr(5,5);  
var examplefourthWord = sentence.substr(11,2); 
var examplefifthWord = sentence.substr(14,2);
var examplesixthWord = sentence.substr(17,5);
var exampleseventhWord = sentence.substr(23,6);
var exampleeighthWord = sentence.substr(30,9);

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + exampleSecondWord); 
console.log('Third Word: ' + examplethirdWord); 
console.log('Fourth Word: ' + examplefourthWord); 
console.log('Fifth Word: ' + examplefifthWord); 
console.log('Sixth Word: ' + examplesixthWord); 
console.log('Seventh Word: ' + exampleseventhWord); 
console.log('Eighth Word: ' + exampleeighthWord)

// soal 3
var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var examplesecondWord2 = sentence2.substring(4, 14); 
var examplethirdWord2 = sentence2.substring(15, 17); 
var examplefourthWord2 = sentence2.substring(18, 20);
var examplefifthWord2 = sentence2.substring(21, 25); 

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + examplesecondWord2); 
console.log('Third Word: ' + examplethirdWord2); 
console.log('Fourth Word: ' + examplefourthWord2); 
console.log('Fifth Word: ' + examplefifthWord2);

// soal 4
var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14);
var thirdWord3 = sentence3.substring(15, 17); 
var fourthWord3 = sentence3.substring(18, 20); 
var fifthWord3 = sentence3.substring(21, 25); 

var firstWordLength = exampleFirstWord3.length  
var secondWordLength = secondWord3.length
var thirdWordLength = thirdWord3.length
var fourthWordLength = fourthWord3.length 
var fifthWordLength = fifthWord3.length

console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length:' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length:' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3 + ', with length:' + fifthWordLength); 
