import React from "react";
import { View, StyleSheet, Text, TouchableOpacity, TextInput, Image}  from 'react-native'

export default function LoginScreen() {
    return (
        <View style={styles.container}>
            <Text style={styles.headerText}>Anntasca Shop</Text>
            <Text style={styles.title}>Log In</Text>
            <View style={styles.form}>
                <View style={styles.formContainer}>
                    <Text style={styles.formTitle}>Username/Email</Text>
                    <View style={styles.formFieldContainer}>
                        <TextInput style={styles.formFieldInput}/>
                    </View>
            </View>
        </View>
        <View style={styles.footer}>
            <TouchableOpacity style={styles.button}>
                <Text style={styles.buttonText}>Log in</Text>
            </TouchableOpacity>
        </View>
        </View>
    );
}

const styles = StyleSheet.create ({
    container: {
        flex: 1
    },
    headerText: {
        alignSelf: "center",
        fontSize: 48,
        fontWeight: "bold"
    },
    title: {
        fontSize: 36,
        fontWeight: "bold"
    },
    form: {
        flex: 1
    },
    formContainer: {
        formTitle: {
            fontSize: 24,
            fontWeight: "bold"
        }
    },
})