// Soal Nomor 1
console.log("Nomor 1")

const golden = () => {
    console.log("this is golden!")
}
golden();

console.log("\n")
// Soal Nomor 2
console.log("Nomor 2")

const firstName = "William";
const lastName = "Imoh";
const fullName = "William Imoh"

const theString = `{
firstName : '${firstName}', 
lastName : '${lastName}', 
fullName : [Function: Fullname],
}
${fullName}`;

console.log(theString);

console.log("\n")
// Soal nomor 3
console.log("Nomor 3")

let newObject = {
    firstname: "Harry",
    lastname: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
};
const { firstname, lastname, destination, occupation, spell } = newObject;

console.log(firstname, lastname, destination, occupation, spell)

console.log("\n")
// Soal nomor 4 
console.log("Nomor 4")

let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east]

console.log(combined)

console.log("\n")
// Soal nomor 5
console.log("Nomor 5")

const planet = "earth"
const view = "glass"

const before = 'Lorem ' + `${view}` + ' dolor sit amet, ' +  
    'consectetur adipiscing elit, ' + `${planet}` + ' do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 
console.log(before) 