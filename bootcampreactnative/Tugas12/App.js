import React from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import Telegram from './Tugas/Tugas12/Telegram';


export default function App() {
    return (
        <Telegram />
    );
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        color: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    }
})