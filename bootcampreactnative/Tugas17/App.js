import React from "react";
import { StyleSheet, Text, View }  from 'react-native';
import Telegram from './Tugas/Tugas12/Telegram';
import RestApi from './Tugas/Tugas17/RestApi';

export default function App() {
    return (
        <RestApi />
    );
}

const styles = StyleSheet.create ({
    container: {
        flex: 1,
        color: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    }
})