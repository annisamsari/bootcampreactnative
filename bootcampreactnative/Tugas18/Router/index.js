import React from 'react'
import { ImagePropTypes, StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';

import AboutScreen from '../Pages/AboutScreen';
import AddScreen from '../Pages/AddScreen';
import Home from '../Pages/Home';
import Login from '../Pages/Login';
import ProjectScreen from '../Pages/ProjectScreen';
import Setting from '../Pages/Setting';
import SkillProject from '../Pages/SkillProject';


const Tab = createBottomTabNavigator ();
const Drawwer = createDrawerNavigator ();
const Stack = createStackNavigator ();


export default function Router() {
    return (
      <NavigationContainer>
          <Stack.Navigator>
              <Stack.Screen name="Login Screen" component={Login}/>
              <Stack.Screen name="Home Screen" component={Home}/>
              <Stack.Screen name="MainApp" component={MainApp}/>
              <Stack.Screen name="MyDrawwer" component={MyDrawwer}/>

          </Stack.Navigator>
      </NavigationContainer>
    )
}

const MainApp =()=>(
    <Tab.Navigator>
        <Tab.Screen name="Home Screen" component={Home}/>
        <Tab.Screen name="Add Screen" component={AddScreen}/>
        <Tab.Screen name="Skill Project" component={SkillProject}/>
        <Tab.Screen name="About Screen" component={AboutScreen}/>
    </Tab.Navigator>
)

const MyDrawwer =()=>(
    <Drawwer.Navigator>
        <Drawwer.Screen name="App" component={MainApp}/>
        <Drawwer.Screen name="About Screen" component={AboutScreen}/>
    </Drawwer.Navigator>
)
