import React from "react";
import { View, StyleSheet, Text, TextInput, Image, Button }  from 'react-native'

export default function Login({navigation}) {
    return (
        <View style={styles.container}>
            <Text style={styles.headerText}>Anntasca Shop</Text>
            <Text>Log In</Text>
            <Button onPress={()=>navigation.navigate("MyDrawwer", {
                screen: 'App' , params:{
                    screen: 'About Screen' 
                }
            })} 
            title="Menuju"/>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})