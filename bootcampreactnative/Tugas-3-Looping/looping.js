// Looping While
console.log("Looping Pertama")

let number = 0
while(number <= 18) {
    number += 2;
    console.log(number + "- I love coding");
}
var n = "\n"
console.log(n);

console.log("Looping Kedua")

let number2 = 22
while(number2 >= 4) {
    number2 -= 2;
    console.log(number2 + "- I will become a mobile developer");
}
var n = "\n"
console.log(n)

// Looping For
console.log("Looping For")

for (let number = 1; number <=20; number++) {
    if(number % 2 === 0) {
        console.log(number + "- Berkualitas");
    }else if(number % 2 !=0 && number % 3 === 0) {
        console.log(number + "- I love Coding");
    }else{
        console.log(number + "- Santai")
    }
}

var n = "\n"
console.log(n)

console.log("Persegi Panjang")

var buff = "";
for (var i= 1; i <=8*4; i++) {
    buff += "#";
    if (i % 8 === 0) {
        buff += "\n";
    }
}
console.log(buff);

var n = "\n"
console.log(n)

console.log("Tangga")

