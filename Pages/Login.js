import React from "react";
import { View, StyleSheet, Text, TextInput, Image, Button }  from 'react-native'

export default function Login({navigation}) {
    return (
        <View style={styles.container}>
            <Text style={styles.headerText}>Welcome</Text>
            <Text style={styles.sectionTitle}>Please Login to Continue</Text>
            <Button onPress={()=>navigation.navigate("Perlo", {
                screen: 'Home' , params:{
                    screen: 'Aboutme' 
                }
            })} 
            title="Login"/>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#2C3251'
    },
    headerText: {
        fontSize: 40,
        color: "white",
        paddingHorizontal: 30,
        paddingVertical: 80
    },
    sectionTitle: {
      fontSize: 24,
      color: "white"
    },
})