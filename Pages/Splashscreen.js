import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

export default function Splashscreen() {
    return (
        <View style={styles.container}>
            <Text style={styles.headerText}>Perlo</Text>
            <Text style={styles.sectionTitle}>List Your To-Do List</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor: '#2C3251',
    },
    headerText: {
      color: "white",
      fontSize: 48,
      paddingHorizontal: 130,
      paddingVertical: 100
    },
    sectionTitle: {
      fontSize: 24,
      color: 'white',
      paddingHorizontal: 80,
      paddingVertical: 110
    },
})